<?php
namespace app\models;
use Yii;

class Entity extends base\Entities
{
	public $type=0;
	public function rules(){
		return [
			[["type"],"integer","min"=>1],
		];
	}
	public function attributeLabels()
	{
		return [
			'type'   => 'Type',
		];
	}
	public function getTypeName(){
		$types=$this->types;
		return $types[$this->type];
	}
	public function getTypes(){
		$types[]="";
		$types[]="Dog";
		$types[]="Cat";
		return $types;
	}
    public function getFields()
    {
        return $this->hasMany(Field::className(), ['entity_id' => 'id']);
    }
}