<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "fields".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $index
 * @property integer $type
 * @property integer $entity_id
 *
 * @property Entities $entity
 */
class Fields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'entity_id'], 'required'],
            [['index', 'type', 'entity_id'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['value'], 'string', 'max' => 2048],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['entity_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'index' => 'Index',
            'type' => 'Type',
            'entity_id' => 'Entity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entities::className(), ['id' => 'entity_id']);
    }
}
