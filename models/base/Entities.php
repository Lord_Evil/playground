<?php

namespace app\models\base;

use Yii;

/**
 * This is the model class for table "entities".
 *
 * @property integer $id
 *
 * @property Fields[] $fields
 */
class Entities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(Fields::className(), ['entity_id' => 'id']);
    }
}
