<?php

namespace app\models;

use Yii;

class APIExceptionNoCall extends \Exception
{
	public function __construct($status = "404", $message = "API Call is not yet implemented", $code = 0, \Exception $previous = null)
    {
        $this->statusCode = $status;
        parent::__construct($message,$status);
    }
}