<?php

namespace app\models;

use Yii;

class APIException extends \Exception
{
	public function __construct($status = 0, $message = null, $code = 0, \Exception $previous = null)
    {
        $this->statusCode = $status;
        parent::__construct($message,$status);
    }
}