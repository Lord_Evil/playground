<?php

namespace app\models;

use Yii;

class ErrorHandler extends \yii\web\ErrorHandler{
    protected function renderException($exception){
		header("Content-Type: application/json");
		header("Access-Control-Allow-Origin: *");
		if($exception instanceof APIExceptionNoCall||$exception instanceof APIException){
			http_response_code(200);
			echo json_encode(['status'=>$exception->getCode(),'message'=>$exception->getMessage()], JSON_PRETTY_PRINT);
		}
		elseif($exception instanceof yii\web\NotFoundHttpException)
			echo json_encode(['status'=>404,'message'=>'Not Found'], JSON_PRETTY_PRINT);
		elseif($exception instanceof yii\web\ForbiddenHttpException)
			echo json_encode(['status'=>403,'message'=>'You are not allowed to perform this action.'], JSON_PRETTY_PRINT);
		elseif($exception instanceof yii\web\UnauthorizedHttpException)
			echo json_encode(['status'=>401,'message'=>'You are requesting with an invalid credential.'], JSON_PRETTY_PRINT);
		elseif($exception instanceof yii\web\BadRequestHttpException)
			echo json_encode(['status'=>400,'message'=>'Malformed Request'], JSON_PRETTY_PRINT);
		elseif($exception instanceof yii\base\ErrorException){
			echo json_encode(['status'=>500,'message'=>$exception->getMessage(),"file"=>$exception->getFile(),"line"=>$exception->getLine()], JSON_PRETTY_PRINT);
		}elseif($exception instanceof yii\base\UnknownClassException){
			echo json_encode(['status'=>500,'message'=>$exception->getMessage(),"file"=>$exception->getFile(),"line"=>$exception->getLine()], JSON_PRETTY_PRINT);
		}
		else parent::renderException($exception);
    }
}