<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;

use app\models\Entity;
use app\models\Field;

class MainController extends Controller{
    public function runAction($id, $params=array()){
        $_POST=Yii::$app->request->post();
        if(!$_POST) $_POST=array();
        $result=parent::runAction($id, $params);
        if(is_array($result)) Yii::$app->response->format='json';
        return $result;
    }
    public function actionOptions(){
        $headers = Yii::$app->response->headers;
        $headers->set('Access-Control-Allow-Origin', '*');
        $headers->set('Access-Control-Allow-Methods', 'GET, POST');
        $headers->set('Access-Control-Allow-Headers', 'Origin, Authorization, X-Requested-With, Content-Type, Accept');
    }
    public $layout="new";
    public $enableCsrfValidation = false;
	public function actionIndex(){
		$items=Entity::find()->with("fields")->asArray()->all();
		return $this->render("index",["list"=>$items]);
	}
    public function actionManage(){
        if(Yii::$app->request->method=="GET"){
            $model=new Entity();
        }elseif(Yii::$app->request->method=="POST"){
            $model=new Entity();
            $model->load($_POST);
            if($model->save()){
                foreach ($_POST["Field"] as $f) {
                    $f["entity_id"]=$model->primaryKey;
                    $f["type"]=1;
                    $fm=new Field();
                    $fm->load(["Field"=>$f]);
                    $fm->save();
                }
                return $this->redirect("index");
            }
        }
        return $this->render("entities",["model"=>$model]);
    }
    public function actionTest(){
        return ["a"=>1];
    }
}