<?php

$vendorDir = dirname(__DIR__);

return [
  'yiisoft/yii2-debug' => 
  [
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    [
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ],
  ],
  [
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    [
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ],
  ],
  'yiisoft/yii2-gii' => 
  [
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    [
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ],
  ],
];
