<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
	$this->title="Entities Manager";
?>
<div style="width:400px;margin:50px 0px 0px 20px">
<?php $form = ActiveForm::begin([
	'id'             => 'role-form',
	'layout'         => 'horizontal',
	'validateOnBlur' => false,
]) ?>
<style type="text/css">
	.field-block .form-control{
		width: 180px;
		display: inline;
	}
</style>
<?= Html::button('<span class="glyphicon glyphicon-plus-sign"></span> ' . "Add",['class' => 'btn btn-default','onclick'=>'addField();','style'=>'float:right']) ?>
<?= $form->field($model, 'type')->dropDownList($model->types) ?>
<div id="fieldList"></div>
<?= Html::submitButton('<span class="glyphicon glyphicon-plus-sign"></span> ' . "Create",['class' => 'btn btn-success','style'=>'float:right']) ?>
<?php ActiveForm::end() ?>
</div>
<script type="text/javascript">
	function addField(){
		var count = $("#role-form .field-block").length;
		var holder=document.createElement("div");
		holder.classList.add("field-block");
		var fieldName=document.createElement("input");
		fieldName.name="Field["+count+"][name]";
		fieldName.classList.add("form-control");
		fieldName.placeholder="Field Name";
		var fieldVal=document.createElement("input");
		fieldVal.name="Field["+count+"][value]";
		fieldVal.classList.add("form-control");
		fieldVal.placeholder='Value';
		holder.appendChild(fieldName);
		holder.appendChild(fieldVal);
		fieldList.appendChild(holder);
	}	
</script>