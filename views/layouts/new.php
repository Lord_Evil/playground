<?php
use app\assets\AppAsset;
AppAsset::register($this);
use yii\helpers\Html;
?>
<!DOCTYPE html>
<?php $this->beginPage() ?>
<html>
	<head>
<?php $this->head() ?>

		<title><?= Html::encode(Yii::$app->name." - ".$this->title) ?></title>
	</head>
<?php $this->beginBody() ?>
<body>
<?= $content ?>

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>