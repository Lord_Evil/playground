<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
    ];
    public $js = [
        //'js/site.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\SiteAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
