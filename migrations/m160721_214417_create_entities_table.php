<?php

use yii\db\Migration;

/**
 * Handles the creation for table `entities`.
 */
class m160721_214417_create_entities_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('entities', [
            'id' => $this->primaryKey(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('entities');
    }
}
