<?php

use yii\db\Migration;

/**
 * Handles the creation for table `fields`.
 * Has foreign keys to the tables:
 *
 * - `entities`
 */
class m160721_214429_create_fields_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('fields', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'value' => $this->string(2048),
            'index' => $this->integer()->notNull()->defaultValue(1),
            'type' => $this->integer()->notNull()->defaultValue(1),
            'entity_id' => $this->integer(),
        ]);

        // creates index for column `entity_id`
        $this->createIndex(
            'idx-fields-entity_id',
            'fields',
            'entity_id'
        );

        // add foreign key for table `entities`
        $this->addForeignKey(
            'fk-fields-entity_id',
            'fields',
            'entity_id',
            'entities',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `entities`
        $this->dropForeignKey(
            'fk-fields-entity_id',
            'fields'
        );

        // drops index for column `entity_id`
        $this->dropIndex(
            'idx-fields-entity_id',
            'fields'
        );

        $this->dropTable('fields');
    }
}
